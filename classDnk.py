import numpy as np
import random

def gen_dnk_letter(): #генерирую случайную букву
    return Dnk.dnk_letters[round(random.random()*len(Dnk.dnk_letters)-1)]

class Dnk():
    dnk_letters = np.array(["R", "L", "G", "J", "A", "S", "s", "D", "C", "M", "H", "F"])
    dnk_size = 64

    def gen_random_dnk(): #генерирую случайное днк
        dnk = np.zeros((64, 2), dtype=object)
        dnk[:, 0] = np.random.choice(Dnk.dnk_letters, 64) # генерирую случайные буквы
        dnk[:, 1] = np.random.randint(0, 63, size=64) # генерирую случайные цифры
        return dnk

    def __new__(cls, *args, **kwargs):
        return super().__new__(cls)
        
    def __init__(self):
        self.data = Dnk.gen_random_dnk()
        self.count = round(random.random()*(Dnk.dnk_size-1))
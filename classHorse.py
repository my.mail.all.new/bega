from baseClasses import Solid, Life, Air, Effect
from classDnk import Dnk
from classCorpse import Corpse
import numpy as np
import random

def gen_dnk_letter(): #генерирую случайную букву
    return Dnk.dnk_letters[round(random.random()*len(Dnk.dnk_letters)-1)]

def mutation_dnk(dnk):
    if round(random.random()) == 1:
        dnk[round(random.random()*(Dnk.dnk_size-1))][0] = gen_dnk_letter()
    else:
        dnk[round(random.random()*(Dnk.dnk_size-1))][1] = round(random.random()*(Dnk.dnk_size-1))
    return dnk 

class Horse(Solid, Life): # моб
    
    povorot = [{"logo": "Я","attak_logo": "<", "y": 0, "x":-1},{"logo": "A","attak_logo": "^", "y":-1 , "x":0}, {"logo": "R","attak_logo": ">", "y":0 , "x":1}, {"logo": "Ɐ","attak_logo": "v", "y": 1, "x":0}]
    colors = "colors_parent"
    horses = np.array([])       #тут храню список все объекты этого класса

    def __new__(cls, *args, **kwargs):
        return super().__new__(cls)
        
    def __init__(self, map_, y=None, x=None, dnk=None, parent=None):
        self.turn = 1
        self.map_ = map_
        self.dammage = False

        self.dnk1 = Dnk()
        
        self.y_speed = 1
        self.health = Horse.health

        if parent:
            
            self.mutant_count = parent.mutant_count
            self.health = parent.health
            self.y = parent.y + Horse.povorot[parent.turn]["y"] 
            self.x = parent.x + Horse.povorot[parent.turn]["x"] 
            
            self.dnk1 = Dnk()
            self.dnk1.data = parent.dnk1.data.copy()

            self.colors_clan = parent.colors_clan
            self.colors_parent = parent.colors_parent.copy()
            self.mutant()
            
        else:
            self.mutant_count = 0
            self.colors_clan = np.random.randint(0, 256, size=3)
            self.colors_parent = self.colors_clan.copy()
            self.y = y
            self.x = x
        
        Horse.horses = np.append(Horse.horses, self)   

    def directly(self):   # скип хода
        pass

    def right(self):      # Повернуться направо
        if self.turn == 3:
            self.turn = 0
        else:
            self.turn += 1 
        self.logo = Horse.povorot[self.turn]["logo"]
        self.attak_logo = Horse.povorot[self.turn]["attak_logo"]

    def left(self):       # Повернуться налево
        if self.turn == 0:
            self.turn = 3
        else:
            self.turn += -1 
        self.logo = Horse.povorot[self.turn]["logo"]
        self.attak_logo = Horse.povorot[self.turn]["attak_logo"]

    def go(self):         # сделать шаг     
        self.logo = Horse.povorot[self.turn]["logo"]
        self.move(y=self.y+Horse.povorot[self.turn]["y"],x=self.x + Horse.povorot[self.turn]["x"])
            
    def jump(self):       # подпрыгнуть
        self.y_speed = -1

    def y_move(self):     # переместиться по y (+гравитация)
        self.move(y=self.y + self.y_speed,x=self.x)
        if not self.y_speed == 1: 
            self.y_speed += 1
        return self.y

    def move(self, y, x): # потом доделать (чтоб сократить код)
        if self.map_.map[y][x].soft:
            self.map_.map[y][x] = self.map_.map[self.y][self.x] 
            self.map_.map[self.y][self.x] = Air()
            self.x = x 
            self.y = y
            return True
        else:
            return False    
            
    def draw(self):       # получить цветной символ
        match Horse.colors:
            case "colors_dnk":
                xzy = xxhash.xxh64(seed=20141025)
                xzy.update(self.dnk1.data)
                xzy1 = str(xzy.intdigest())
                return f"\033[48;2;{round(int(xzy1[:3])/4)};{round(int(xzy1[3:6])/4)};{round(int(xzy1[6:9])/4)}m{self.logo}\033[0m"
            case "colors_clan":
                return f"\033[48;2;{self.colors_clan[0]};{self.colors_clan[1]};{self.colors_clan[2]}m{self.logo}\033[0m"
            case "colors_parent":
                return f"\033[48;2;{self.colors_parent[0]};{self.colors_parent[1]};{self.colors_parent[2]}m{self.logo}\033[0m"
            case "colors_health":
                if self.health <= 255:
                    return f"\033[48;2;{255-self.health};{self.health};{0}m{self.logo}\033[0m"
                else:
                    return f"\033[48;2;{0};{0};{255}m{self.logo}\033[0m"     
            case "colors_mutant":
                if self.mutant_count <= 8:
                    return f"\033[48;2;{self.mutant_count*32-1};{self.mutant_count*32-1};{0}m{self.logo}\033[0m"
                elif self.mutant_count <= 16:
                    return f"\033[48;2;{0};{0};{self.mutant_count*16-1}m{self.logo}\033[0m"
                else:
                    return f"\033[48;2;{255};{0};{0}m{self.logo}\033[0m"

    def brain(self):      # мозг
        match self.dnk1.data[self.dnk1.count][0]:
            case "L":
                self.left()
            case "R":
                self.right()
            case "G":
                 self.go()
            case "J":
                self.jump()
            case "A":
                self.attak()
            case "S":
                self.see()
            case "s":
                self.see2()
            case "D":
                self.directly()
            case "C":
                self.child()
            case "M":
                self.mutant()
            case "H":
                self.see_hp()
            case "F":
                self.photosynthesis()
        self.dnk1.count = self.dnk1.data[self.dnk1.count][1]
    
    def see_hp(self):     # посмотреть хп
        if self.health > 300:
            self.dnk1.count = 10
        elif self.health > 200:
            self.dnk1.count = 11
        elif self.health > 100:
            self.dnk1.count = 12
        elif self.health > 50:
            self.dnk1.count = 13
        elif self.health > 25:
            self.dnk1.count = 14 

    def see(self):        # посмотреть
        cell = self.map_.map[self.y + Horse.povorot[self.turn]["y"]][self.x + Horse.povorot[self.turn]["x"]] 
        if cell.soft:
            self.dnk1.count = 1
        elif cell.alive:
            razn = abs(self.colors_parent[0]-cell.colors_parent[0])
            razn += abs(self.colors_parent[1]-cell.colors_parent[1])
            razn += abs(self.colors_parent[2]-cell.colors_parent[2])
            if razn < 16:
                self.dnk1.count = 2
            elif razn < 32:
                self.dnk1.count = 7
            elif razn < 64:
                self.dnk1.count = 8
            elif razn < 128:
                self.dnk1.count = 9
        else:
            self.dnk1.count = 3
        self.logo = "Q"

    def see2(self):       # посмотреть
        if self.y + Horse.povorot[self.turn]["y"] + Horse.povorot[self.turn]["y"] < self.map_.height and self.x + Horse.povorot[self.turn]["x"] + Horse.povorot[self.turn]["x"] < self.map_.width:
            cell = self.map_.map[self.y + Horse.povorot[self.turn]["y"] + Horse.povorot[self.turn]["y"] ][self.x + Horse.povorot[self.turn]["x"] + Horse.povorot[self.turn]["x"] ] 
            if cell.soft:
                self.dnk1.count = 15
            elif cell.alive:
                razn = abs(self.colors_parent[0]-cell.colors_parent[0])
                razn += abs(self.colors_parent[1]-cell.colors_parent[1])
                razn += abs(self.colors_parent[2]-cell.colors_parent[2])
                if razn < 16:
                    self.dnk1.count = 16
                elif razn < 32:
                    self.dnk1.count = 17
                elif razn < 64:
                    self.dnk1.count = 18
                elif razn < 128:
                    self.dnk1.count = 19
            else:
                self.dnk1.count = 20
            self.logo = "Q"

    def attak(self):      # атакт
        def vampir(mob, dammage):
            mob.dnk1.count = 0 # рефлекс урона
            mob.health += -dammage 
            self.health += dammage
        cell1 = self.map_.map[self.y+Horse.povorot[self.turn]["y"]][self.x+Horse.povorot[self.turn]["x"]]     
        if cell1.soft:
            cell2 = self.map_.map[self.y+Horse.povorot[self.turn]["y"]+Horse.povorot[self.turn]["y"]][self.x+Horse.povorot[self.turn]["x"]+Horse.povorot[self.turn]["x"]]
            self.logo = Horse.povorot[self.turn]["attak_logo"]
            self.map_.map[self.y+Horse.povorot[self.turn]["y"]][self.x+Horse.povorot[self.turn]["x"]] = Effect(Horse.povorot[self.turn]["attak_logo"])
            if cell2.alive:
                vampir(mob=cell2, dammage=round(random.random()*12+4))
        elif cell1.alive:
            vampir(mob=cell1, dammage=round(random.random()*4+4))

    def mutant(self):     # поменять 1 случайную бакву в днк и 1 случайное число 
        self.mutant_count += 1
        self.logo = "*"
        self.dnk1.data = mutation_dnk(self.dnk1.data)
        def new_color(color):
            color_num = 2
            
            if round(random.random()) == 1:
                if color + color_num < 256:
                    return color + color_num
                else:
                    return  color - color_num
            else:
                if color - color_num > 0:
                    return  color - color_num
                else:
                    return  color + color_num

        self.colors_parent[0] = new_color(self.colors_parent[0])
        self.colors_parent[1] = new_color(self.colors_parent[1])
        self.colors_parent[2] = new_color(self.colors_parent[2])

    def dead(self):       # умереть (переделать надо)
        #if self.mutant_count > 16:
        #    if self.mutant_count * random.random() > 15:   
        if self.mutant_count >= 16: 
            if random.random()*500 < 1:
                self.map_.map[self.y][self.x] = Corpse(self)
                self.alive = False
                return True
        if self.health < 0:
            self.map_.map[self.y][self.x] = Effect("@")
            self.alive = False
            return True
        else:   
            return False

    def photosynthesis(self):
        self.logo = "F"
        if self.y <= 20:
            self.health += 1
        else:
            self.health += -1
    def child(self):      # оставить потомка
        self.logo = "X"
        if self.turn and self.map_.map[self.y][self.x].health >= 16:
            if self.map_.map[self.y + Horse.povorot[self.turn]["y"]][self.x + Horse.povorot[self.turn]["x"]].soft:
                self.logo = "O" 
                self.health = round(self.health/2)
                self.mutant_count = round(self.mutant_count/2)-1
                self.map_.map[self.y + Horse.povorot[self.turn]["y"]][self.x + Horse.povorot[self.turn]["x"]] = Horse(map_=self.map_, parent=self)
                self.dnk1.count = 4
            else:
                self.dnk1.count = 5
        else:
            self.dnk1.count = 6

    def actions(self):    # 
        if random.random()*10 > 9:
            self.health += -1
        
        #if self.y >= 35:
        #    self.health += -2
        if self.dead():
            return True
        else:
            if random.random()*500 < 1:
                self.mutant()
            else:
                #self.y_move() 
                self.brain()
            return False
class Base():
    act = False
    logo = "*"
    def draw(self):
        return self.logo

    def actions():
        pass

class Solid(): #сквозь нельзя проходить
    soft = False

class Soft(): #сквозь можно проходить
    soft = True

class Life(Base): #живой
    alive = True
    health = 255

class Death(Base): #предмет
    alive = False

class Block(Solid, Death): #блок 
    logo = "#"

class Air(Soft, Death): #воздух
    logo = " "

class Effect(Soft, Death): #эффект (прозрачный, будет пропадать со временем)

    def __new__(cls, *args, **kwargs):
        return super().__new__(cls)
        
    def __init__(self, logo):
        self.logo = logo
        
from baseClasses import Block, Air
from classDnk import Dnk
from classHorse import Horse

import numpy as np
import random

class Map(): # карта

    def __new__(cls, *args, **kwargs):
        return super().__new__(cls)
        
    def __init__(self, height, width):
        self.height = height
        self.width = width
        self.map = np.array([]) 
        self.map = np.full((height, width), Air())
        
        self.generate_borders()
        self.generate_mountains()
        
    def generate_borders(self):
        self.map[0, :] = Block()
        self.map[-1, :] = Block()
        self.map[:, 0] = Block()
        self.map[:, -1] = Block()

    def draw_map(self):
        for y in self.map:
            for x in y:
                print(x.draw(),end="")
            print()

    def generate_mountains(self): # заполнить поле горами
        for y in range(self.height-2, 0, -1):
            for x in range(self.width-1):
                if not self.map[y+1][x-1].soft and not self.map[y+1][x].soft and not self.map[y+1][x+1].soft:
                    if not self.map[y+1][x-1].alive and not self.map[y+1][x].alive and not self.map[y+1][x+1].alive:
                        if random.random() < 0.85:
                            self.map[y][x] = Block()

    def spawn_one(self, y=None, x=None, dnk=None):  # заспавнить 1 моба
        self.dnk = dnk if dnk else Dnk.gen_random_dnk()
        y = y if y else round(random.random()*(self.height-1))
        x = x if x else round(random.random()*(self.width-1))
        if self.map[y][x].soft:          
            self.map[y][x] = Horse(map_=self, y=y, x=x, dnk=dnk)
            return True
        else:
            return False
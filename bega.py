import random
import time
import os
import numpy as np
import keyboard
import copy
from baseClasses import Base, Soft, Solid, Death, Life, Air, Block, Effect
import xxhash
from classDnk import Dnk
from classCorpse import Corpse
from classMap import Map
from classHorse import Horse
print("1.5")

start = True
height = 43 #высота поля
width = 190 #ширина поля

def set_colors(col):
    
    Horse.colors = col
    os.system('clear')
    map1.draw_map()

def foo_dnk():
    set_colors("colors_dnk")
def foo_clan():
    set_colors("colors_clan")
def foo_parent():
    set_colors("colors_parent")
def foo_health():
  set_colors("colors_health")
def colors_mutant():
  set_colors("colors_mutant")
def pause():
    global start
    start = False if start else True
    print("Пауза")

keyboard.add_hotkey('Ctrl + 3', foo_dnk)
keyboard.add_hotkey('Ctrl + 9', foo_clan)
keyboard.add_hotkey('Ctrl + 5', foo_parent)
keyboard.add_hotkey('Ctrl + 6', foo_health)
keyboard.add_hotkey('Ctrl + 7', colors_mutant)
keyboard.add_hotkey('Ctrl + 8', pause)

def spawn_random(count=10, map_=None): # заспавнить в случайных местах
    map_ = map_ if map_ else Map(height, width)
    def spawn():
        if not map_.spawn_one():
            spawn()
    for i in range(count): 
        spawn()
    return map_

def otbor_new():
    global start, i_time, otbor_time
    start = True
    while i_time<int(otbor_time):
        if start:
            i_time += 1
            #for y in map1.map:
            #    for x in y:
            #        if x.act:
            #            x.actions()
            for j in range(len(Horse.horses)): # прохожусь по всем мобам
                Horse.horses[j].actions()
            horses1=np.array([])
            for horse1 in Horse.horses: # оставляю только живых мобов
                if horse1.alive:
                    horses1 = np.append(horses1, horse1)
            Horse.horses = horses1
            energ = 0
            for _ in Horse.horses:
                energ += _.health
            os.system('clear')
            print(f"time: {i_time}        horses:{len(Horse.horses)}        E:{energ}        Q - see    A - directly    (O - division    X - no division)       colors: {Horse.colors}")
            map1.draw_map()
            time.sleep(0.05)

i_time = 0
map1 = Map(height, width)
numb = input("number of mobs: ")
otbor_time = input("otbor_time: ")
spawn_random(count=int(numb), map_=map1)
map1.draw_map()

otbor_new()
#for horse in Horse.horses:
#    print("\n============================================")
#    print(horse)
#    print(horse.dnk1)
#    for i in horse.dnk1.data:
#        print(i[0],end="")
#        print(i[1],end="")
#рефлексы: 0 - получил урон, (1, 3) - зрение, (4, 5, 6) - размножение, 2 7 8 9 - посмотреть цвет, (10,11,12,13,14) -посмотреть хп

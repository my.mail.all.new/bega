from baseClasses import Solid, Death, Air

class Corpse(Solid, Death):
    act = True
    logo = "▪"

    def __new__(cls, *args, **kwargs):
        return super().__new__(cls)
        
    def __init__(self, parent):
        self.energ = parent.health
        self.y = parent.y 
        self.x = parent.x
        self.map_ = parent.map_

    def draw(self):
        return Corpse.logo

    def actions(self):
        if self.map_.map[self.y+1][self.x].soft: 
            self.map_.map[self.y+1][self.x] = self
            self.map_.map[self.y][self.x] = Air()
            self.y += 1
